# Angular 2 Skeleton Starter Kit (A2SSK)

## This Repo

This is a basic Angular 2(4) Skeleton project. It has all the **latest** and **greatest** dependencies to get you up and running.

## Installation

### GIT

Install with `git`:

    >   $ git clone git@github.com:Chevon-Phillip/Angular2-Skeleton-Starter-Kit-A2SSK.git
    >   $ cd Angular2-Skeleton-Starter-Kit-A2SSK
    >   $ npm start

### NPM

Install with `npm`:

    >   $ mkdir Angular2-Skeleton-Starter-Kit-A2SSK
    >   $ cd Angular2-Skeleton-Starter-Kit-A2SSK
    >   $ npm install angular2-skeleton@1.0.1
    >   $ npm start

## License

The [**MIT**](https://opensource.org/licenses/MIT) License.